# [Chroma] Code Highlighting in GitBook

This is an alternative syntax highlighting plugin.

Due to repeatedly calling an external process, this plugin slows down the build.


Requires [Chroma] to be installed and `chroma` to be accessible.

*book.json*:

```json
"plugins": ["chroma"]
```

Run `generate.sh` before use, unless [installing from npm].

Based on the [pygments plugin](https://plugins.gitbook.com/plugin/pygments)

[chroma]: https://github.com/alecthomas/chroma
[installing from npm]: https://www.npmjs.com/package/gitbook-plugin-chroma
