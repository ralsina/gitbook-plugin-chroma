var child_process = require('child_process');


function highlight(lang, code) {
    if (!lang) {
        return {
            body: code,
            html: false
        };
    }

    try {
        args = ['-l', lang, '--html-only',
            '--formatter=html', '--style', 'vs'
        ]

        lines = code.split('\n')
        while (lines[lines.length - 1] == '') {
            lines.pop()
        }
        if (lines.length && lines[0].startsWith('|||')) {
            // Highlighted lines
            start = lines.shift()
            args.push('--html-highlight=' + start.slice(3))
        }
        if (lines.length && lines[0].startsWith('&&&')) {
            // Start number prefix
            start = lines.shift()
            args.push('--html-lines')
            args.push('--html-base-line=' + start.slice(3))
        } else if (lines.length > 1) {
            args.push('--html-lines')
        }

        code = lines.join('\n')

        if (lang != 'text') {
            code = child_process.execFileSync(
                'chroma', args, {
                    input: code,
                    encoding: 'utf-8'
                })
        }
    } catch (e) {
        if (e instanceof TypeError) {
            console.error("`chroma` may be missing!");
        }
        if (!e.stderr || e.stderr.indexOf("no lexer") == -1) {
            throw e;
        }
    }

    return code;
}


module.exports = {
    book: {
        assets: './css',
        css: ['website.css']
    },
    ebook: {
        assets: './css',
        css: ['ebook.css']
    },
    blocks: {
        code: function (block) {
            return highlight(block.kwargs.language, block.body);
        }
    }
};
